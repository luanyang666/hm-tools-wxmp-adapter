package top.hmtools.wxmp.message.template.model;

/**
 * Auto-generated: 2019-08-29 11:48:4
 *
 * @author bejson.com (i@bejson.com)
 * @website http://www.bejson.com/java2pojo/
 */
public class DataItem {

	private String value;
	private String color;
	
	public DataItem(String value,String color ) {
		this.color = color;
		this.value = value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public String getValue() {
		return value;
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}


}