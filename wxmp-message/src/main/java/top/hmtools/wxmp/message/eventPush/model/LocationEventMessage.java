package top.hmtools.wxmp.message.eventPush.model;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.annotations.XStreamAlias;

import top.hmtools.wxmp.core.annotation.WxmpMessage;
import top.hmtools.wxmp.core.model.message.BaseEventMessage;
import top.hmtools.wxmp.core.model.message.enums.Event;
import top.hmtools.wxmp.core.model.message.enums.MsgType;

/**
 * 用户地理位置事件消息
 * {@code
 * <xml>
  <ToUserName><![CDATA[toUser]]></ToUserName>
  <FromUserName><![CDATA[fromUser]]></FromUserName>
  <CreateTime>123456789</CreateTime>
  <MsgType><![CDATA[event]]></MsgType>
  <Event><![CDATA[LOCATION]]></Event>
  <Latitude>23.137466</Latitude>
  <Longitude>113.352425</Longitude>
  <Precision>119.385040</Precision>
</xml>
 * }
 * @author Hybomyth
 *
 */
@WxmpMessage(msgType=MsgType.event,event=Event.LOCATION)
public class LocationEventMessage extends BaseEventMessage{

	/**
	 * 
	 */
	private static final long serialVersionUID = -279750089825821519L;

	/**
	 * 地理位置纬度
	 */
	@XStreamAlias("Latitude")
	private Double latitude;
	
	/**
	 * 地理位置经度
	 */
	@XStreamAlias("Longitude")
	private Double longitude;
	
	/**
	 * 地理位置精度
	 */
	@XStreamAlias("Precision")
	private Double precision;

	public Double getLatitude() {
		return latitude;
	}

	public void setLatitude(Double latitude) {
		this.latitude = latitude;
	}

	public Double getLongitude() {
		return longitude;
	}

	public void setLongitude(Double longitude) {
		this.longitude = longitude;
	}

	public Double getPrecision() {
		return precision;
	}

	public void setPrecision(Double precision) {
		this.precision = precision;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	@Override
	public void configXStream(XStream xStream) {
		
	}

	
}
