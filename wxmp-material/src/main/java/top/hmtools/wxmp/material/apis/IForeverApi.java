package top.hmtools.wxmp.material.apis;

import java.io.InputStream;

import top.hmtools.wxmp.core.annotation.WxmpApi;
import top.hmtools.wxmp.core.annotation.WxmpMapper;
import top.hmtools.wxmp.core.enums.HttpMethods;
import top.hmtools.wxmp.core.enums.HttpParamType;
import top.hmtools.wxmp.core.model.ErrcodeBean;
import top.hmtools.wxmp.material.model.BatchgetMaterialParam;
import top.hmtools.wxmp.material.model.BatchgetMaterialResult;
import top.hmtools.wxmp.material.model.MaterialCountResult;
import top.hmtools.wxmp.material.model.MaterialResult;
import top.hmtools.wxmp.material.model.MediaBean;
import top.hmtools.wxmp.material.model.MediaParam;
import top.hmtools.wxmp.material.model.NewsBean;
import top.hmtools.wxmp.material.model.NewsBeanForUpdate;
import top.hmtools.wxmp.material.model.UploadParam;

@WxmpMapper
public interface IForeverApi {

	/**
	 * 新增永久图文素材
	 * @param newsBean
	 * @return
	 */
	@WxmpApi(httpMethods=HttpMethods.POST,uri="/cgi-bin/material/add_news")
	public MediaBean addNews(NewsBean newsBean);
	
	/**
	 * 上传图文消息内的图片获取URL
	 * <br>本接口所上传的图片不占用公众号的素材库中图片数量的5000个的限制。图片仅支持jpg/png格式，大小必须在1MB以下。
	 * @param uploadParam
	 * @return
	 */
	@WxmpApi(httpMethods=HttpMethods.POST,uri = "/cgi-bin/media/uploadimg",httpParamType=HttpParamType.FORM_DATA)
	public MediaBean uploadImageForNews(UploadParam uploadParam);
	
	/**
	 * 新增其他类型永久素材
	 * @param uploadParam
	 * @return
	 */
	@WxmpApi(httpMethods=HttpMethods.POST,uri = "/cgi-bin/material/add_material",httpParamType=HttpParamType.FORM_DATA)
	public MediaBean addMaterial(UploadParam uploadParam);
	
	/**
	 * 获取永久素材
	 * <br>图文、视频
	 * @param mediaParam
	 * @return
	 */
	@WxmpApi(httpMethods=HttpMethods.POST,uri="/cgi-bin/material/get_material")
	public MaterialResult getMaterial(MediaParam mediaParam);

	/**
	 * 获取永久素材
	 * <br>图片
	 * @param mediaParam
	 * @return
	 */
	@WxmpApi(httpMethods=HttpMethods.POST,uri="/cgi-bin/material/get_material")
	public InputStream getMaterialImage(MediaParam mediaParam);
	
	/**
	 * 删除永久素材
	 * <br>1、请谨慎操作本接口，因为它可以删除公众号在公众平台官网素材管理模块中新建的图文消息、语音、视频等素材（但需要先通过获取素材列表来获知素材的media_id）
	 * <br>2、临时素材无法通过本接口删除
	 * <br>3、调用该接口需https协议	
	 * @param mediaParam
	 * @return
	 */
	@WxmpApi(httpMethods=HttpMethods.POST,uri="/cgi-bin/material/del_material")
	public ErrcodeBean delMaterial(MediaParam mediaParam);
	
	/**
	 * 修改永久图文素材
	 * <br>1、也可以在公众平台官网素材管理模块中保存的图文消息（永久图文素材）
	 * @param newsBean
	 * @return
	 */
	@WxmpApi(httpMethods=HttpMethods.POST,uri="/cgi-bin/material/update_news")
	public ErrcodeBean updateNews(NewsBeanForUpdate newsBean);
	
	/**
	 * 获取素材总数
	 * <br>1.永久素材的总数，也会计算公众平台官网素材管理中的素材
	 * <br>2.图片和图文消息素材（包括单图文和多图文）的总数上限为5000，其他素材的总数上限为1000
	 * 
	 * @return
	 */
	@WxmpApi(httpMethods=HttpMethods.GET,uri="/cgi-bin/material/get_materialcount")
	public MaterialCountResult getMaterialCount();
	
	/**
	 * 获取素材列表
	 * <br>1、获取永久素材的列表，也包含公众号在公众平台官网素材管理模块中新建的图文消息、语音、视频等素材
	 * <br>2、临时素材无法通过本接口获取
	 * @param batchgetMaterialParam
	 * @return
	 */
	@WxmpApi(httpMethods=HttpMethods.POST,uri="/cgi-bin/material/batchget_material")
	public BatchgetMaterialResult getBatchgetMaterial(BatchgetMaterialParam batchgetMaterialParam);
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}
