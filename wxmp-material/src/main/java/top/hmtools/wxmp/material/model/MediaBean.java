package top.hmtools.wxmp.material.model;

import top.hmtools.wxmp.core.model.ErrcodeBean;
import top.hmtools.wxmp.material.enums.MediaType;

/**
 * 临时素材实体类
 * @author Hybomyth
 *
 */
public class MediaBean extends ErrcodeBean{

	/**
	 * 媒体文件类型，分别有图片（image）、语音（voice）、视频（video）和缩略图（thumb，主要用于视频与音乐格式的缩略图）
	 */
	private String type;
	
	/**
	 * 媒体文件上传后，获取标识
	 */
	private String media_id;
	
	/**
	 * 为永久图文素材上传图片后返回的图片URL路径
	 */
	private String url;
	
	/**
	 * 媒体文件上传时间戳
	 */
	private long created_at;

	public String getType() {
		return type;
	}
	
	public MediaType getEnumType(){
		if(this.type!=null && this.type.length()>0){
			return MediaType.valueOf(this.type);
		}else{
			return null;
		}
	}

	public void setType(String type) {
		this.type = type;
	}
	
	public void setType(MediaType type) {
		this.type = type.toString();
	}

	public String getMedia_id() {
		return media_id;
	}

	public void setMedia_id(String media_id) {
		this.media_id = media_id;
	}

	public long getCreated_at() {
		return created_at;
	}

	public void setCreated_at(long created_at) {
		this.created_at = created_at;
	}
	
	

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	@Override
	public String toString() {
		return "MediaBean [type=" + type + ", media_id=" + media_id + ", url=" + url + ", created_at=" + created_at
				+ ", errcode=" + errcode + ", errmsg=" + errmsg + "]";
	}


}
