package top.hmtools.wxmp.core;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alibaba.fastjson.JSON;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import top.hmtools.wxmp.core.annotation.WxmpController;
import top.hmtools.wxmp.core.annotation.WxmpRequestMapping;
import top.hmtools.wxmp.core.eventModels.NamingVerifyFail;
import top.hmtools.wxmp.core.eventModels.NamingVerifySuccess;
import top.hmtools.wxmp.core.eventModels.TextMessage;

@WxmpController
public class WxmpControllerTest {
	
	final Logger logger = LoggerFactory.getLogger(WxmpControllerTest.class);
	private ObjectMapper objectMapper;

	/**
	 * 用于处理TextMessage消息的具体处理方法
	 * @param textMessage
	 * @return
	 */
	@WxmpRequestMapping
	public String executeTextMessage(TextMessage textMessage){
		this.printFormatedJson("获取的入参内容是", textMessage);
		return "top.hmtools.wxmp.core.WxmpControllerTest.executeTextMessage(TextMessage) 执行OK，消息内容是："+textMessage.getContent();
	}
	
	/**
	 * 用于处理`名称认证成功（即命名成功）`消息的具体处理方法
	 * @param namingVerifySuccess
	 * @return
	 */
	@WxmpRequestMapping
	public NamingVerifySuccess executeNamingVerifySuccess(NamingVerifySuccess namingVerifySuccess){
		this.printFormatedJson("获取的入参内容是", namingVerifySuccess);
		return namingVerifySuccess;
	}
	
	/**
	 * 用于处理`名称认证失败`消息的具体处理方法
	 * @param namingVerifyFail
	 * @return
	 */
	@WxmpRequestMapping
	public NamingVerifyFail executeNamingVerifyFail(NamingVerifyFail namingVerifyFail){
		this.printFormatedJson("获取的入参内容是", namingVerifyFail);
		return namingVerifyFail;
	}
	
	
	
	/**
	 * 格式化打印json字符串到控制台
	 * @param title
	 * @param obj
	 */
	protected synchronized void printFormatedJson(String title,Object obj) {
		if(this.objectMapper == null){
			this.objectMapper = new ObjectMapper();
		}
		try {
			//阿里的fastjson具有很好的兼容性，所以才多次一举
			String jsonString = JSON.toJSONString(obj);
			Object tempObj = JSON.parse(jsonString);
			String formatedJsonStr = this.objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(tempObj);
			this.logger.info("\n{}：\n{}",title,formatedJsonStr);
		} catch (JsonProcessingException e) {
			this.logger.error("格式化打印json异常：",e);
		}
	}
}
