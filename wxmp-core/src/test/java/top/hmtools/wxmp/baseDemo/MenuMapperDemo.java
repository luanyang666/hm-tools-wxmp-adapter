package top.hmtools.wxmp.baseDemo;

import top.hmtools.wxmp.baseDemo.model.MenuWapperDemoBean;
import top.hmtools.wxmp.core.annotation.WxmpApi;
import top.hmtools.wxmp.core.annotation.WxmpMapper;
import top.hmtools.wxmp.core.enums.HttpMethods;

/**
 * 1、接口必须被 `@WxmpMapper` 注解修饰
 * @author HyboWork
 *
 */
@WxmpMapper
public interface MenuMapperDemo {

	/**
	 * 查询自定义菜单目录
	 * <br>2、方法必须被 `@WxmpApi`注解修饰，其中：httpMethods是指定http请求微信接口的方法（必须的）；uri 是请求微信接口的URI地址（必须的）
	 * <br>3. MenuWapperDemoBean 是用于自动反序列化请求微信接口返回数据为实体类对象实例。
	 * @return
	 */
	@WxmpApi(httpMethods=HttpMethods.GET, uri = "/cgi-bin/menu/get")
	public MenuWapperDemoBean getMenu();
}
