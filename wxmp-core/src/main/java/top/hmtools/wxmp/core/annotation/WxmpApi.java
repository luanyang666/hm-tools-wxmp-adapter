package top.hmtools.wxmp.core.annotation;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import org.springframework.core.annotation.AliasFor;

import top.hmtools.wxmp.core.RequestParamConvert;
import top.hmtools.wxmp.core.RequestParamConvertDefault;
import top.hmtools.wxmp.core.enums.HttpMethods;
import top.hmtools.wxmp.core.enums.HttpParamType;

@Target({ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface WxmpApi {

	/**
	 * http请求方法，有GET\POST等
	 * @return
	 */
	HttpMethods httpMethods() default HttpMethods.POST;
	
	/**
	 * http请求参数数据组织结构，有json、xml等
	 * @return
	 */
	HttpParamType httpParamType() default HttpParamType.JSON_BODY;
	
	@AliasFor("value")
	String uri();
	
	/**
	 * 请求的微信接口URI
	 * @return
	 */
	@AliasFor("uri")
	String value() default "";
	
	/**
	 * 请求参数转换工具
	 * @return
	 */
	Class<? extends RequestParamConvert> convert() default RequestParamConvertDefault.class;
}
