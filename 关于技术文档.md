#### 说明
1. 技术文档基于vuepress构建纯静态页面，使用gitee pages作为静态页面空间。
2. vuepress官网：https://www.vuepress.cn/
3. htdocs 为技术文档根目录，技术文档原文件及构建后的整站均在此目录下。
4. package.json 为nodejs构建配置文件。

#### 构建
1. 全局安装 vuepress
```
npm install -g vuepress
```
2. 创建 package.json
```
{
  "scripts": {
    "dev": "vuepress dev htdocs",
    "build": "vuepress build htdocs"
  }
}
```
其中 htdocs 为技术文档构建根目录。

3. 开发环境运行：
```
npm run dev
```

4. 生产环境编译
```
npm run build
```

5. icp备案信息
- 执行完 `npm run build`后，将 
```
<div style="text-align: center;height: 50px;line-height: 50px;">
    <a href="http://beian.miit.gov.cn/">湘ICP备15000533号-2</a>
</div>
```
复制到 $/.vuepress/dist/index.html 文件中即可

6. 背景音乐
- 使用网易云音乐生成外链，然后也是将相关代码放置 index.html 中。